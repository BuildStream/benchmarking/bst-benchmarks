#!/usr/bin/env python3

from contextlib import suppress
from csv import DictWriter
import functools
import json
import os
from pathlib import Path
import shutil
import subprocess
import sys
import time
from typing import Callable, Dict, List, Optional, Sequence, Tuple, Union

import click
from tqdm import tqdm as _tqdm, trange
import yaml

import report


ROOT_PATH = Path(__file__).resolve().parent
CONFIGURATIONS_PATH = ROOT_PATH.joinpath("configs")
CACHE_PATH = ROOT_PATH.joinpath("cache")
TOX_WORKDIR_PATH = CACHE_PATH.joinpath("tox")
BUILDSTREAM_CACHE = CACHE_PATH.joinpath("bst-cache")

BUILDSTREAM_PATH = Path(
    os.getenv("BST_REPOSITORY", CACHE_PATH.joinpath("buildstream"))
)
BUILDSTREAM_REMOTE = "https://github.com/apache/buildstream.git"

PROJECT_PATH = CACHE_PATH.joinpath("project")
PROJECT_REMOTE = (
    "https://gitlab.com/BuildStream/benchmarking/debian-stretch-bst.git"
)
PROJECT_BRANCH = "origin/bst2"


def tqdm(sequence: Sequence, **kwargs):
    if len(sequence) == 1:
        disable = True
    else:
        disable = None

    kwargs.update({"disable": disable})
    return _tqdm(sequence, **kwargs)


class Configuration:  # pylint: disable=too-few-public-methods
    @classmethod
    def list(cls) -> List[str]:
        return [path.stem for path in CONFIGURATIONS_PATH.glob("*")]

    def __init__(self, config_name: str) -> None:
        config_path = CONFIGURATIONS_PATH.joinpath(config_name + ".json")
        with config_path.open() as fhandle:
            data = json.load(fhandle)

        self.builders = data["builders"]
        self.files = data["files"]
        self.runs = data["runs"]
        self.warmups = data["warmups"]

        self.python_versions = [
            version
            if version != "current"
            else "py{}{}".format(
                sys.version_info.major, sys.version_info.minor
            )
            for version in data["python-versions"]
        ]


class Action:
    def cleanup(self) -> None:
        pass

    def get_command(self, bst: str, elements: List[str]) -> List[str]:
        raise NotImplementedError()

    def __str__(self):
        raise NotImplementedError()


class ShowAction(Action):
    def __init__(self, type_: Optional[str] = None):
        self.type = type_

    def get_command(self, bst: str, elements: List[str]) -> List[str]:
        return [bst, "show", *elements]

    def __str__(self):
        if self.type:
            return "show - {}".format(self.type)
        return "show"


class SourceFetchAction(Action):
    def cleanup(self):
        with suppress(FileNotFoundError):
            shutil.rmtree(str(BUILDSTREAM_CACHE))

    def get_command(self, bst, elements):
        return [bst, "source", "fetch", *elements]

    def __str__(self):
        return "source fetch"


class BuildAction(Action):
    def __init__(self, n_builders: int) -> None:
        self.n_builders = n_builders

    def cleanup(self):
        with suppress(FileNotFoundError):
            shutil.rmtree(str(BUILDSTREAM_CACHE))

    def get_command(self, bst, elements):
        return [bst, "--builders", str(self.n_builders), "build", *elements]

    def __str__(self):
        return "build - {}".format(self.n_builders)


def format_time_secs(secs):
    if secs < 60:
        return "{:.1f} secs".format(secs)
    return "{} mins, {} secs".format(*divmod(int(secs), 60))


def run(cmd, cwd: Optional[Path] = None, env: Optional[Dict[str, str]] = None):
    try:
        return subprocess.check_output(
            cmd,
            cwd=None if cwd is None else str(cwd),
            stderr=subprocess.STDOUT,
            env=env,
            universal_newlines=True,
        )
    except subprocess.CalledProcessError as exc:
        print(exc.stdout, file=sys.stderr)
        raise


def time_command(command: List[str], cwd: Union[Path, str]) -> Tuple[str, str]:
    environ = os.environ.copy()
    environ["XDG_CACHE_HOME"] = str(BUILDSTREAM_CACHE)
    environ["XDG_CONFIG_HOME"] = str(BUILDSTREAM_CACHE)

    # GNU time may be installed as 'gtime' instead, so prefer to use that. This
    # would be the case if we're running on MacOSX and the user has installed
    # `gnu-time` with Homebrew.
    time_path = shutil.which("gtime")
    if time_path is None:
        time_path = shutil.which("time")

    output = (
        run(
            [time_path, "--format", "%e %M", "--", *command],
            cwd=cwd,
            env=environ,
        )
        .strip()
        .splitlines()
    )

    return output[-1].split(" ")


def update_buildstream_mirror():
    print("Updating buildstream mirror at '{}'.".format(BUILDSTREAM_PATH))
    if not BUILDSTREAM_PATH.exists():
        subprocess.check_call(
            [
                "git",
                "clone",
                "--mirror",
                BUILDSTREAM_REMOTE,
                str(BUILDSTREAM_PATH),
            ]
        )
    else:
        subprocess.check_call(
            ["git", "remote", "update"], cwd=BUILDSTREAM_PATH
        )


def update_project():
    print("Updating project at '{}'.".format(PROJECT_PATH))
    if not PROJECT_PATH.exists():
        subprocess.check_call(
            ["git", "clone", PROJECT_REMOTE, str(PROJECT_PATH)]
        )
    else:
        subprocess.check_call(["git", "fetch", "--prune"], cwd=PROJECT_PATH)

    subprocess.check_call(
        ["git", "reset", "--hard", PROJECT_BRANCH], cwd=str(PROJECT_PATH)
    )
    prepare_project(PROJECT_PATH)


def prepare_project(path):
    conf_path = path.joinpath("project.conf")
    conf = yaml.load(conf_path.read_text(), Loader=yaml.SafeLoader)
    project_dir = conf_path.resolve().parent
    conf["aliases"]["url_to_project"] = "file://{}/".format(project_dir)
    conf_path.write_text(yaml.dump(conf))


def get_commits_between(git_repo: Path, base: str, head: str) -> List[str]:
    all_commits = (
        run(["git", "rev-list", "{}..{}".format(base, head)], cwd=git_repo)
        .strip()
        .splitlines()
    )
    commits = [head]
    commits.extend(all_commits[1:-1])
    commits.append(base)
    return commits


def get_commit_sha(commit: str):
    return run(
        ["git", "rev-parse", "--short", "--verify", commit],
        cwd=BUILDSTREAM_PATH,
    ).strip()


def create_tox_environment(python_version: str, commit: str) -> Path:
    environ = os.environ.copy()
    environ["BST_COMMIT"] = commit
    environ["BST_REPOSITORY"] = str(BUILDSTREAM_PATH)

    tox_path = TOX_WORKDIR_PATH.joinpath(commit)

    run(
        ["tox", "--notest", "--workdir", str(tox_path), "-e", python_version],
        env=environ,
    )

    return tox_path


def benchmark(
    configuration: Configuration,
    get_commits: Callable[..., List[str]],
    show_only: bool,
):  # pylint: disable=too-many-locals
    # rewriting this function to have fewer local vars does not result in
    # cleaner code
    update_buildstream_mirror()
    update_project()

    commits = get_commits()
    commit_shas = [get_commit_sha(commit) for commit in commits]

    actions = [ShowAction()]  # type: List[Action]

    if not show_only:
        actions.append(SourceFetchAction())
        actions.append(ShowAction("sources fetched"))

        for builders in configuration.builders:
            actions.append(BuildAction(builders))

        actions.append(ShowAction("cached"))

    runs = configuration.runs + configuration.warmups

    timings = []

    with tqdm(commit_shas, leave=False) as tqdm_commits:
        for commit_ref, commit_sha in zip(commits, tqdm_commits):
            if commit_ref.startswith(commit_sha):
                commit = commit_ref
            else:
                commit = "{} - {}".format(commit_ref, commit_sha)

            tqdm_commits.set_description("commit '{}'".format(commit))

            tqdm_python_version = tqdm(
                configuration.python_versions, leave=False
            )
            for python_version in tqdm_python_version:
                tqdm_python_version.set_description(
                    "python '{}'".format(python_version)
                )

                tox_path = create_tox_environment(python_version, commit_sha)

                # Ensure each start of a run is clean
                with suppress(FileNotFoundError):
                    shutil.rmtree(str(BUILDSTREAM_CACHE))

                bst_command = tox_path.joinpath(python_version, "bin", "bst")
                tqdm_actions = tqdm(actions, leave=False)
                for action in tqdm_actions:
                    tqdm_actions.set_description("action '{}'".format(action))

                    for iteration in trange(
                        runs, desc="iteration", leave=False
                    ):

                        action.cleanup()

                        timing = time_command(
                            action.get_command(
                                str(bst_command), configuration.files
                            ),
                            cwd=PROJECT_PATH,
                        )

                        if iteration >= configuration.warmups:
                            timings.append(
                                {
                                    "commit": commit,
                                    "python_version": python_version,
                                    "action": str(action),
                                    "max_memory": timing[1],
                                    "time": timing[0],
                                }
                            )

    return timings


@click.command()
@click.option(
    "--config", type=click.Choice(Configuration.list()), default="quick"
)
@click.option(
    "--report-path",
    type=click.Path(file_okay=True, writable=True),
    default=os.path.join(os.getcwd(), "report.csv"),
)
@click.option("--between", is_flag=True)
@click.option("--show-only", is_flag=True)
@click.argument("reference")
@click.argument("candidate")
def compare(
    config: str,
    reference: str,
    candidate: str,
    report_path: str,
    between: bool,
    show_only: bool,
):
    if between:
        get_commits = functools.partial(
            get_commits_between, base=reference, head=candidate
        )
    else:

        def get_commits():
            return [reference, candidate]

    loaded_config = Configuration(config)

    start_time = time.time()
    results = benchmark(loaded_config, get_commits, show_only=show_only)
    stop_time = time.time()

    # Note that when copy and pasting the output as markdown, the '-' bullets
    # here make it more legible, by keeping things on separate lines.
    print()
    print("- Finished in:", format_time_secs(stop_time - start_time))
    print("- Number of builders:", loaded_config.builders)
    print("- Target files:", " ".join(loaded_config.files))
    print("- Number of runs:", loaded_config.runs)
    print("- Number of warmups:", loaded_config.warmups)
    print("- Python versions:", " ".join(loaded_config.python_versions))
    print()

    with open(report_path, "w") as fhandle:
        writer = DictWriter(fhandle, results[0].keys())

        writer.writeheader()
        writer.writerows(results)

    report.show_report(report_path)


if __name__ == "__main__":
    #  pylint: disable=no-value-for-parameter
    # pylint doesn't agree with this use of click commands
    compare()
