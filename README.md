# BST Benchmark

## Running on MacOS

Note that `time` works differently on MacOS than on most Linux distros.

To get the behaviour expected by this repo, you could install the GNU version
with Homebrew:

```bash
brew install gnu-time
```

This would make `gtime` available on your $PATH.
