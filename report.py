import click
import pandas as pd
from tabulate import tabulate


def mean_std_str(col):
    std = round(col.std(), 2)

    if not std:
        std_str = ""
    else:
        std_str = " ± {:.2f}".format(std)
    return "{:.2f}".format(col.mean()) + std_str


def print_dataframe(df):
    values = df.values.tolist()

    last_step = None
    for value in values:
        if value[0] == last_step:
            value[0] = ""
        else:
            last_step = value[0]

    print(tabulate(values, headers=df.columns.values, tablefmt="pipe"))


def show_report(report_file):
    df = pd.read_csv(report_file)

    original_num_rows = len(df)

    df["max_memory"] /= 1024

    df.rename(
        {"max_memory": "max memory (MB)", "time": "time (secs)"},
        axis="columns",
        inplace=True,
    )

    df = (
        df.groupby(["action", "python_version", "commit"])
        .agg(
            **{
                "median max memory (MB)": ("max memory (MB)", "median"),
                "mean max memory (MB) ± std": (
                    "max memory (MB)",
                    mean_std_str,
                ),
                "median time (secs)": ("time (secs)", "median"),
                "mean time (secs) ± std": ("time (secs)", mean_std_str),
            }
        )
        .reset_index()
    )

    new_num_rows = len(df)

    time_df = df[
        [
            "action",
            "python_version",
            "commit",
            "median time (secs)",
            "mean time (secs) ± std",
        ]
    ]
    memory_df = df[
        [
            "action",
            "python_version",
            "commit",
            "median max memory (MB)",
            "mean max memory (MB) ± std",
        ]
    ]

    print_dataframe(time_df)
    print()
    print_dataframe(memory_df)
    print()
    print(
        "There were {} runs of each command.".format(
            original_num_rows // new_num_rows
        )
    )


@click.command()
@click.argument("report_file", type=click.Path("r"))
def report(report_file):
    show_report(report_file)


if __name__ == "__main__":
    report()
